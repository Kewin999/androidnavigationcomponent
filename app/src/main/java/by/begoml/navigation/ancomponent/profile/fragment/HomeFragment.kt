package by.begoml.navigation.ancomponent.profile.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import by.begoml.navigation.ancomponent.R
import by.begoml.navigation.ancomponent.base.BaseFragment

class HomeFragment : BaseFragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun getTitleResId(): Int {
        return R.string.title_home
    }

    override fun onBackPressed(): Boolean {
        AlertDialog.Builder(activity!!)
            .setMessage("Do you really want to leave?")
            .setPositiveButton("Yes") { _, _ ->
                activity?.finish()
            }
            .setNegativeButton("No") { _, _ ->

            }
            .show()
        return true
    }
}