package by.begoml.navigation.ancomponent.base

import androidx.appcompat.app.AppCompatActivity
import by.begoml.navigation.ancomponent.base.listener.OnToolbarListener

abstract class BaseActivity : AppCompatActivity(), OnToolbarListener