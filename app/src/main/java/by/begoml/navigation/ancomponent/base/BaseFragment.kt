package by.begoml.navigation.ancomponent.base

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import by.begoml.navigation.ancomponent.base.listener.OnBackPressedListener
import by.begoml.navigation.ancomponent.base.listener.OnToolbarListener

abstract class BaseFragment : Fragment(), OnBackPressedListener {

    private var toolbarListener: OnToolbarListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnToolbarListener) {
            toolbarListener = context
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val titleResId = getTitleResId()
        if (titleResId > 0) {
            toolbarListener?.setToolbarTitle(titleResId)
        }
    }

    override fun onBackPressed(): Boolean {
        return false
    }

    abstract fun getTitleResId(): Int
}