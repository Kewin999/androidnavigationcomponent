package by.begoml.navigation.ancomponent.start.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import by.begoml.navigation.ancomponent.R
import by.begoml.navigation.ancomponent.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_start.*

class StartFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_start, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnGoToLogin.setOnClickListener {
            val action = StartFragmentDirections.goToProfile(userId = 99)
            findNavController().navigate(action)
        }

        btnGoToDialogInfo.setOnClickListener {
            findNavController().navigate(R.id.goToStartDialogInfo)
        }

        btnGoToDialogInfoParam.setOnClickListener {
            val action = StartFragmentDirections.goToStartDialogInfo(
                userMsg = "custom user message",
                userTitle = "custom user title"
            )
            findNavController().navigate(action)
        }
    }

    override fun getTitleResId(): Int {
        return R.string.app_name
    }
}