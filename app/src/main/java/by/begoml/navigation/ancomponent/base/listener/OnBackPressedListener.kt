package by.begoml.navigation.ancomponent.base.listener

interface OnBackPressedListener {

    fun onBackPressed(): Boolean
}