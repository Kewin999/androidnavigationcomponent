package by.begoml.navigation.ancomponent.profile.fragment.news

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import by.begoml.navigation.ancomponent.R
import by.begoml.navigation.ancomponent.base.BaseFragment

class NewsDetailtsFragment : BaseFragment() {


    override fun getTitleResId(): Int {
        return 0
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_news_details, container, false)
    }


}