package by.begoml.navigation.ancomponent.start


import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import by.begoml.navigation.ancomponent.R


/**
 * An activity that inflates a layout that has a NavHostFragment.
 */
class StartActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)
    }
}