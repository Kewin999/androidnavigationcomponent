package by.begoml.navigation.ancomponent.base.listener

import androidx.annotation.StringRes

interface OnToolbarListener {

    fun setToolbarTitle(@StringRes titleResId: Int)
}