package by.begoml.navigation.ancomponent.profile.dialog

import android.app.Dialog
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.navArgs
import by.begoml.navigation.ancomponent.R


class StartInfoDialogFragment : DialogFragment() {

    val args: StartInfoDialogFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val theme = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            android.R.style.Theme_Material_Light_NoActionBar
        else
            android.R.style.Theme_DeviceDefault_Light_NoActionBar
        setStyle(STYLE_NO_TITLE, theme)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val msg = args.userMsg
        val title = args.userTitle
        val adb = AlertDialog.Builder(context!!)
        adb.setView(R.layout.dialog_fragment_start_info)
        adb.setMessage(msg)
        adb.setTitle(title)
        return adb.create()
    }

}