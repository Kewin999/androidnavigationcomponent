package by.begoml.navigation.ancomponent.profile.fragment.news

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import by.begoml.navigation.ancomponent.R
import by.begoml.navigation.ancomponent.base.BaseFragment

class NewsFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_news, container, false)
    }

    override fun getTitleResId(): Int {
        return R.string.title_news
    }
}