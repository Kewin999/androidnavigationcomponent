package by.begoml.navigation.ancomponent.profile.activity

import android.os.Bundle
import android.view.MenuItem
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.*
import by.begoml.navigation.ancomponent.R
import by.begoml.navigation.ancomponent.base.BaseActivity
import by.begoml.navigation.ancomponent.base.listener.OnBackPressedListener
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.view_toolbar.*


/**
 * An activity that inflates a layout that has a NavHostFragment.
 */
class ProfileActivity : BaseActivity() {

    private lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        //Getting the Navigation Controller
        navController = Navigation.findNavController(this, R.id.navHostFragment)

        //Setting the navigation controller to Bottom Nav
        bottomNav.setupWithNavController(navController)


        // include navigation support with the default action bar
        appBarConfiguration = AppBarConfiguration(navController.graph)
        NavigationUI.setupWithNavController(toolbar, navController, appBarConfiguration)

        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.homeFragment, R.id.newsFragment -> {
                    toolbar.navigationIcon = null
                }
                R.id.settingsFragment -> {
                    toolbar.setNavigationIcon(android.R.drawable.ic_menu_save)
                }
                else -> {
                    toolbar.setNavigationIcon(R.drawable.ic_arror_back)
                }
            }
        }


    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun setToolbarTitle(titleResId: Int) {
        toolbar.title = getString(titleResId)
    }


    override fun onBackPressed() {
        val currentFragment = navHostFragment.childFragmentManager.fragments[0]

        if (currentFragment is OnBackPressedListener && currentFragment.onBackPressed()) {
            return
        } else {

            when (navController.currentDestination?.id ?: 0) {
                R.id.settingsFragment -> {

                }
                else -> {
                    if (!navController.popBackStack()) {
                        finish()
                    }
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return item.onNavDestinationSelected(navController) || super.onOptionsItemSelected(item)
    }
}